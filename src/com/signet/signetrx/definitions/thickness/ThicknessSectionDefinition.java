/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.signet.signetrx.definitions.thickness;

import com.signet.signetrx.definitions.SectionDefinition;

/**
 *
 * @author sburns
 */
public class ThicknessSectionDefinition extends SectionDefinition {
    public ThicknessSectionDefinition() {
        super("TCPIP");
        
        putSectionPropertyDefinition(DeltactetSectionPropertyDefinition.PROPERTY_NAME, new DeltactetSectionPropertyDefinition());
    }
}
