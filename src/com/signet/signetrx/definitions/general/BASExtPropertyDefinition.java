/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.signet.signetrx.definitions.general;

import com.signet.signetrx.definitions.ValidExtensionSectionPropertyDefinition;

/**
 *
 * @author sburns
 */
public class BASExtPropertyDefinition extends ValidExtensionSectionPropertyDefinition {
    
    public static final String PROPERTY_NAME = "BASExt";
    public static final String DEFAULT_VALUE = "lms";
    public static final boolean ACCESS_CONTROLLED = true;
    
    public BASExtPropertyDefinition() {
        super(PROPERTY_NAME, DEFAULT_VALUE, ACCESS_CONTROLLED);
    }
}
