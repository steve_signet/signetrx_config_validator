/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.signet.signetrx.validate;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.signet.commons.logging.LogWriter;
import com.signet.signetrx.definitions.SectionDefinition;
import com.signet.signetrx.definitions.SectionPropertyDefinition;
import com.signet.signetrx.definitions.designs.DesignsSectionDefinition;
import com.signet.signetrx.definitions.exprism.ExPrismSectionDefinition;
import com.signet.signetrx.definitions.general.GeneralSectionDefinition;
import com.signet.signetrx.definitions.iofolders.IOFoldersSectionDefinition;
import com.signet.signetrx.definitions.thickness.ThicknessSectionDefinition;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.ini4j.Ini;
import org.ini4j.Profile.Section;
import org.ini4j.Wini;

/**
 *
 * @author sburns
 */
public class SignetrxConfigValidator {    
    private LogWriter log = com.signet.signetrx.validate.SignetrxConfigValidatorLogWriter.getInstance();
    private String fileName;
    
    public SignetrxConfigValidator(String fileName) {
        this.fileName = fileName;
    }
    
    public void run() {
        if (fileName == null) {
            System.out.println("File name not set");
            return;
        }
                 
        try {    
            Wini ini = new Wini(new File(getFileName()));
            
            Set<String> sectionNameSet = ini.keySet();

            for(String sectionName: sectionNameSet) {
                System.out.println("\nSection Name: " + sectionName); 

                Ini.Section section = ini.get(sectionName);
                Set<String> keySet = section.keySet();
                
                SectionDefinition sectionDefinition = getSectionDefinition(sectionName);
                
                if (sectionDefinition != null) {
                    List<String> invalidSectionPropertyNameList = sectionDefinition.getInvalidSectionPropertyNameList(keySet);

                    for (String invalidSectionPropertyName: invalidSectionPropertyNameList) {
                        System.out.println("Invalid section property name: " +                 
                                invalidSectionPropertyName);       
                    }

                    List<String> missingSectionPropertyNameList = sectionDefinition.getMissingSectionPropertyNameList(keySet);

                    for (String missingSectionPropertyName: missingSectionPropertyNameList) {
                        System.out.println("Missing section property name: " + 
                                missingSectionPropertyName);
                    }

                    List<String> errorMessageList;

                    if (sectionName.equals("EXPRISM") || sectionName.equals("ASBLANKS")) {
                        errorMessageList = validateKeys(sectionDefinition, section);
                    } else {
                        errorMessageList = validate(sectionDefinition, section);
                    }

                    for (String errorMessage: errorMessageList) {
                        System.out.println(errorMessage);                        
                    } 
                }                 
            }            
         } catch (IOException ex) {
            log.error("Error processing RxInfo file: " + ex.getMessage());
        }   
    }
    
    public SectionDefinition getSectionDefinition(String sectionName) {
        SectionDefinition sectionDefinition = null;

        switch (sectionName) {
            case "THICKNESS":
                sectionDefinition = new ThicknessSectionDefinition();
                break;
            case "GENERAL":
                sectionDefinition = new GeneralSectionDefinition();
                break;
            case "IOFOLDER":
                sectionDefinition = new IOFoldersSectionDefinition();
                break;
            case "EXPRISM":
                sectionDefinition = new ExPrismSectionDefinition();
                break;
            case "DESIGNS":
                sectionDefinition = new DesignsSectionDefinition();
                break;
        }
        
        return sectionDefinition;
    }
        
    public List<String> validate(SectionDefinition sectionDefinition, Ini.Section section) {
        List<String> errorMessageList = new ArrayList<>();
        
        Set<String> keySet = section.keySet();

        for (String key: keySet) {               
            String value = section.get(key);

            Map<String, SectionPropertyDefinition> sectionPropertyDefinitionMap = 
                    sectionDefinition.getSectionPropertyDefinitionMap();

            SectionPropertyDefinition sectionPropertyDefinition = sectionPropertyDefinitionMap.get(key);

            if (sectionPropertyDefinition != null) {
                String errorMessage;
                
                errorMessage = sectionPropertyDefinition.validate(value);
                
                // sburns
                //System.out.println("name: " + sectionPropertyDefinition.getName());

                if (errorMessage.equals("") == false) {
                    errorMessageList.add(errorMessage);
                }
            }                    
        }
        
        return errorMessageList;
    }
        
    public List<String> validateKeys(SectionDefinition sectionDefinition, Ini.Section section) {
        List<String> errorMessageList = new ArrayList<>();
        
        Set<String> keySet = section.keySet();

        for (String key: keySet) {               
            Map<String, SectionPropertyDefinition> sectionPropertyDefinitionMap = 
                    sectionDefinition.getSectionPropertyDefinitionMap();

            SectionPropertyDefinition sectionPropertyDefinition = sectionPropertyDefinitionMap.get(key);

            String errorMessage = "Section property: %s is invalid";

            if (sectionPropertyDefinition == null) {
                errorMessage = String.format(errorMessage, key);
                errorMessageList.add(errorMessage);
            }           
         }
        
        return errorMessageList;
    }

    /**
     * @return the fileName
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * @param fileName the fileName to set
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
    
    public static void main(String[] args) {
        //SignetrxConfigValidator SignetrxConfigValidator = new SignetrxConfigValidator("test\\com\\signet\\signetrx\\validate\\100130_Central_RxInfo.ini");        
        SignetrxConfigValidator SignetrxConfigValidator = new SignetrxConfigValidator("test/com/signet/signetrx/validate/100130_Central_RxInfo.ini");
        SignetrxConfigValidator.run();
    }
}
