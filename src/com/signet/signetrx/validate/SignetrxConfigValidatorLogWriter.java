/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.signet.signetrx.validate;

import com.signet.commons.logging.LogWriter;
import java.net.URL;
import org.apache.log4j.LogManager;

/**
 *
 * @author sburns
 */

public class SignetrxConfigValidatorLogWriter extends LogWriter {
    
    private static final URL configUrl = ClassLoader.getSystemResource("com/signet/signetrx/validate/logging.properties");
    
    private static final com.signet.signetrx.validate.SignetrxConfigValidatorLogWriter instance = 
            new com.signet.signetrx.validate.SignetrxConfigValidatorLogWriter();
    
    public SignetrxConfigValidatorLogWriter() {
        super(configUrl);
        super.addLogger(LogManager.getLogger("AppLogFile"));
        super.addLogger(LogManager.getLogger("SystemOut"));
        super.addLogger(LogManager.getLogger("TextPane"));
    }
    
    public static com.signet.signetrx.validate.SignetrxConfigValidatorLogWriter getInstance() {
        return instance;
    }
}
