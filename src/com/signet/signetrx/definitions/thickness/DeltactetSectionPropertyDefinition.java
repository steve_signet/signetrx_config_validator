/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.signet.signetrx.definitions.thickness;

import com.signet.signetrx.definitions.NumericRangeSectionPropertyDefinition;

/**
 *
 * @author sburns
 */
public class DeltactetSectionPropertyDefinition extends NumericRangeSectionPropertyDefinition {
    
    public static final String PROPERTY_NAME = "deltactet";
    public static final String DEFAULT_VALUE = "2.0";
    public static final boolean ACCESS_CONTROLLED = true;
    
    public DeltactetSectionPropertyDefinition() {
        super(PROPERTY_NAME, DEFAULT_VALUE, ACCESS_CONTROLLED);
        this.setRange(0.0, 2000.0);
    }
}
