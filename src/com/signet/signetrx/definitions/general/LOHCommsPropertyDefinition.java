/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.signet.signetrx.definitions.general;

import com.signet.signetrx.definitions.ListSectionPropertyDefinition;

/**
 *
 * @author sburns
 */
public class LOHCommsPropertyDefinition extends ListSectionPropertyDefinition {
    
    public static final String PROPERTY_NAME = "LOHComms";
    public static final String DEFAULT_VALUE = "1";
    public static final boolean ACCESS_CONTROLLED = true;
    
    public LOHCommsPropertyDefinition() {
        super(PROPERTY_NAME, DEFAULT_VALUE, ACCESS_CONTROLLED);
    }
}
