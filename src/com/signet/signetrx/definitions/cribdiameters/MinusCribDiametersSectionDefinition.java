/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.signet.signetrx.definitions.cribdiameters;

import com.signet.signetrx.definitions.SectionDefinition;

/**
 *
 * @author sburns
 */
public class MinusCribDiametersSectionDefinition extends SectionDefinition {
    private String name;
    
    public MinusCribDiametersSectionDefinition(int number) {
        super("MINUS CRIB DIAMETERS");
        
        for (int i = 1; i <= number; i++) {
            name = "crib" + i;
            putSectionPropertyDefinition(name, new CribSectionPropertyDefinition(name));
        }
    }
}
