/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.signet.signetrx.definitions.designs;

import com.signet.signetrx.definitions.SectionDefinition;

/**
 *
 * @author sburns
 */
public class DesignsSectionDefinition extends SectionDefinition {
    public DesignsSectionDefinition() {
        super("DESIGNS");
        
        putSectionPropertyDefinition(NoAntifatiqueSectionPropertyDefinition.PROPERTY_NAME, new NoAntifatiqueSectionPropertyDefinition());
        putSectionPropertyDefinition(NoAtoricSectionPropertyDefinition.PROPERTY_NAME, new NoAtoricSectionPropertyDefinition());
        putSectionPropertyDefinition(NoBiasphericSectionPropertyDefinition.PROPERTY_NAME, new NoBiasphericSectionPropertyDefinition());
        putSectionPropertyDefinition(NoCrossbowsSectionPropertyDefinition.PROPERTY_NAME, new NoCrossbowsSectionPropertyDefinition());
        putSectionPropertyDefinition(NoDigitalSectionPropertyDefinition.PROPERTY_NAME, new NoDigitalSectionPropertyDefinition());
        putSectionPropertyDefinition(NodirectekSectionPropertyDefinition.PROPERTY_NAME, new NodirectekSectionPropertyDefinition());
        putSectionPropertyDefinition(NoGoldSectionPropertyDefinition.PROPERTY_NAME, new NoGoldSectionPropertyDefinition());
        putSectionPropertyDefinition(NoKUSectionPropertyDefinition.PROPERTY_NAME, new NoKUSectionPropertyDefinition());
        putSectionPropertyDefinition(NoOCRSectionPropertyDefinition.PROPERTY_NAME, new NoOCRSectionPropertyDefinition());
        putSectionPropertyDefinition(NoPrismSectionPropertyDefinition.PROPERTY_NAME, new NoPrismSectionPropertyDefinition());
        putSectionPropertyDefinition(NoSoftwearSectionPropertyDefinition.PROPERTY_NAME, new NoSoftwearSectionPropertyDefinition());
        putSectionPropertyDefinition(NoTestSectionPropertyDefinition.PROPERTY_NAME, new NoTestSectionPropertyDefinition());
        putSectionPropertyDefinition(NoVSectionPropertyDefinition.PROPERTY_NAME, new NoVSectionPropertyDefinition());
        putSectionPropertyDefinition(NoVariableDecentSectionPropertyDefinition.PROPERTY_NAME, new NoVariableDecentSectionPropertyDefinition());
    }
}
