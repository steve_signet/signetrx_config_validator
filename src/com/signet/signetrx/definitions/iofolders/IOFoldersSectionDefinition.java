/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.signet.signetrx.definitions.iofolders;

import com.signet.signetrx.definitions.SectionDefinition;

/**
 *
 * @author sburns
 */
public class IOFoldersSectionDefinition extends SectionDefinition {
    public IOFoldersSectionDefinition() {
        super("IOFOLDERS");
        
        putSectionPropertyDefinition(InputDirSectionPropertyDefinition.PROPERTY_NAME, new InputDirSectionPropertyDefinition());
        putSectionPropertyDefinition(OutputDirSectionPropertyDefinition.PROPERTY_NAME, new OutputDirSectionPropertyDefinition());
    }
}
