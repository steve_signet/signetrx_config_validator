/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.signet.signetrx.definitions.general;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author sburns
 */
public class UseXMLInputPropertyDefinitionTest {
    private UseXMLInputPropertyDefinition instance;
    
    public UseXMLInputPropertyDefinitionTest() {
    }
       
    @Before
    public void setUp() {
        instance = new UseXMLInputPropertyDefinition();
    }
    
    @Test
    public void testValidateSucceed() {
        System.out.println("validate");
        String value = "1";
        String expResult = "";
        String result = instance.validate(value);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testValidateFail1() {
        System.out.println("validate");
        String value = "0";
        String expResult = "";
        String result = instance.validate(value);
        assertNotSame(expResult, result);
    }
    
    @Test
    public void testValidateFail2() {
        System.out.println("validate");
        String value = "2";
        String expResult = "";
        String result = instance.validate(value);
        assertNotSame(expResult, result);
    }
}