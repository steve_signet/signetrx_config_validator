/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.signet.signetrx.definitions.tcpip;

import com.signet.signetrx.definitions.ValidIPAddressSectionPropertyDefinition;

/**
 *
 * @author sburns
 */
public class IPAddressSectionPropertyDefinition extends ValidIPAddressSectionPropertyDefinition {
    
    public static final String PROPERTY_NAME = "IPAddress";
    public static final String DEFAULT_VALUE = "127.0.0.1";
    public static final boolean ACCESS_CONTROLLED = true;
    
    public IPAddressSectionPropertyDefinition() {
        super(PROPERTY_NAME, DEFAULT_VALUE, ACCESS_CONTROLLED);
    }
}
