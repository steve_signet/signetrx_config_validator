/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.signet.signetrx.definitions.general;

import com.signet.signetrx.definitions.NumericRangeSectionPropertyDefinition;

/**
 *
 * @author sburns
 */
public class BlockDiaAllowPropertyDefinition extends NumericRangeSectionPropertyDefinition {
    
    public static final String PROPERTY_NAME = "BlockDiaAllow";
    public static final String DEFAULT_VALUE = "1.9";
    public static final boolean ACCESS_CONTROLLED = false;
    
    public BlockDiaAllowPropertyDefinition() {
        super(PROPERTY_NAME, DEFAULT_VALUE, ACCESS_CONTROLLED);
        this.setRange(0, 10);
    }
}
