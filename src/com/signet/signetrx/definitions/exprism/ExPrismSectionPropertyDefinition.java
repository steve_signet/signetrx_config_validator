/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.signet.signetrx.definitions.exprism;

import com.signet.signetrx.definitions.SectionPropertyDefinition;

/**
 *
 * @author sburns
 */
public class ExPrismSectionPropertyDefinition extends SectionPropertyDefinition {
    
    public static final String PROPERTY_NAME = "EXPRISM";
    public static final String DEFAULT_VALUE = "";
    public static final boolean ACCESS_CONTROLLED = true;
    
    public ExPrismSectionPropertyDefinition() {
        super(PROPERTY_NAME, DEFAULT_VALUE, ACCESS_CONTROLLED);
    }

    @Override
    public String validate(String value) {
        throw new UnsupportedOperationException("Not supported.");
    }
}
