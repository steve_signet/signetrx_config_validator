/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.signet.signetrx.definitions.general;

import com.signet.signetrx.definitions.ListSectionPropertyDefinition;

/**
 *
 * @author sburns
 */
public class PrismBlockOrderPropertyDefinition extends ListSectionPropertyDefinition {
    
    public static final String PROPERTY_NAME = "PrismBlockOrder";
    public static final String DEFAULT_VALUE = "0";
    public static final boolean ACCESS_CONTROLLED = false;
    
    public PrismBlockOrderPropertyDefinition() {
        super(PROPERTY_NAME, DEFAULT_VALUE, ACCESS_CONTROLLED);
    }
}
