/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.signet.signetrx.definitions.currency;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author sburns
 */
public class CurrencySectionPropertyDefinitionTest {
    private CurrencySectionPropertyDefinition instance;
    
    public CurrencySectionPropertyDefinitionTest() {
    }
       
    @Before
    public void setUp() {
        instance = new CurrencySectionPropertyDefinition();
    }
    
    @Test
    public void testValidateSucceed() {
        System.out.println("validate");
        String value = "$";
        String expResult = "";
        String result = instance.validate(value);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testValidateFail() {
        System.out.println("validate");
        String value = "%";
        String expResult = "";
        String result = instance.validate(value);
        assertNotSame(expResult, result);
    }
}