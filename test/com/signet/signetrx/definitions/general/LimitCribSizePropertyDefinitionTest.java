/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.signet.signetrx.definitions.general;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author sburns
 */
public class LimitCribSizePropertyDefinitionTest {
    private LimitCribSizePropertyDefinition instance;
    
    public LimitCribSizePropertyDefinitionTest() {
    }
       
    @Before
    public void setUp() {
        instance = new LimitCribSizePropertyDefinition();
    }
    
    @Test
    public void testValidateSucceed1() {
        System.out.println("validate");
        String value = "0";
        String expResult = "";
        String result = instance.validate(value);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testValidateSucceed2() {
        System.out.println("validate");
        String value = "1";
        String expResult = "";
        String result = instance.validate(value);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testValidateFail() {
        System.out.println("validate");
        String value = "2";
        String expResult = "";
        String result = instance.validate(value);
        assertNotSame(expResult, result);
    }
}