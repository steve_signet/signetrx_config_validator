/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.signet.signetrx.definitions.general;

import com.signet.signetrx.definitions.SectionDefinition;
import com.signet.signetrx.definitions.SectionPropertyDefinition;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author sburns
 */
public final class GeneralSectionDefinition extends SectionDefinition {
    public GeneralSectionDefinition() {
        super("GENERAL");
                
        putSectionPropertyDefinition(AddAdjustPropertyDefinition.PROPERTY_NAME, new AddAdjustPropertyDefinition());
        putSectionPropertyDefinition(AddFactorPropertyDefinition.PROPERTY_NAME, new AddFactorPropertyDefinition());        
        putSectionPropertyDefinition(AutoBlockSelectPropertyDefinition.PROPERTY_NAME, new AutoBlockSelectPropertyDefinition());        
        putSectionPropertyDefinition(AutoInsetPropertyDefinition.PROPERTY_NAME, new AutoInsetPropertyDefinition());        
        putSectionPropertyDefinition(BASExtPropertyDefinition.PROPERTY_NAME, new BASExtPropertyDefinition());        
        putSectionPropertyDefinition(BlockDiaAllowPropertyDefinition.PROPERTY_NAME, new BlockDiaAllowPropertyDefinition());        
        putSectionPropertyDefinition(CORXConvertPropertyDefinition.PROPERTY_NAME, new CORXConvertPropertyDefinition());        
        putSectionPropertyDefinition(CreateJobIniPropertyDefinition.PROPERTY_NAME, new CreateJobIniPropertyDefinition());        
        putSectionPropertyDefinition(GAXPropertyDefinition.PROPERTY_NAME, new GAXPropertyDefinition());        
        putSectionPropertyDefinition(LimitCribSizePropertyDefinition.PROPERTY_NAME, new LimitCribSizePropertyDefinition());        
        putSectionPropertyDefinition(LimitMinusCribDiametersPropertyDefinition.PROPERTY_NAME, new LimitMinusCribDiametersPropertyDefinition());        
        putSectionPropertyDefinition(LMLFilesPropertyDefinition.PROPERTY_NAME, new LMLFilesPropertyDefinition());        
        putSectionPropertyDefinition(LMSFilePropertyDefinition.PROPERTY_NAME, new LMSFilePropertyDefinition());        
        putSectionPropertyDefinition(LogFileFreqPropertyDefinition.PROPERTY_NAME, new LogFileFreqPropertyDefinition());        
        putSectionPropertyDefinition(LOHCommsPropertyDefinition.PROPERTY_NAME, new LOHCommsPropertyDefinition());        
        putSectionPropertyDefinition(LowClickWarningPropertyDefinition.PROPERTY_NAME, new LowClickWarningPropertyDefinition());        
        putSectionPropertyDefinition(MaxBlockPrismPropertyDefinition.PROPERTY_NAME, new MaxBlockPrismPropertyDefinition());        
        putSectionPropertyDefinition(MaxConvexAddPropertyDefinition.PROPERTY_NAME, new MaxConvexAddPropertyDefinition());        
        putSectionPropertyDefinition(MaxSurfacePrismPropertyDefinition.PROPERTY_NAME, new MaxSurfacePrismPropertyDefinition());        
        putSectionPropertyDefinition(ModFilenamePropertyDefinition.PROPERTY_NAME, new ModFilenamePropertyDefinition());        
        putSectionPropertyDefinition(PrismBlockOrderPropertyDefinition.PROPERTY_NAME, new PrismBlockOrderPropertyDefinition());        
        putSectionPropertyDefinition(RotateOutputPropertyDefinition.PROPERTY_NAME, new RotateOutputPropertyDefinition());        
        putSectionPropertyDefinition(SDFFilePropertyDefinition.PROPERTY_NAME, new SDFFilePropertyDefinition());        
        putSectionPropertyDefinition(SDFToRXServerPropertyDefinition.PROPERTY_NAME, new SDFToRXServerPropertyDefinition());        
        putSectionPropertyDefinition(SmdMaxThresholdPropertyDefinition.PROPERTY_NAME, new SmdMaxThresholdPropertyDefinition());        
        putSectionPropertyDefinition(SWRXConvertPropertyDefinition.PROPERTY_NAME, new SWRXConvertPropertyDefinition());        
        putSectionPropertyDefinition(TimeoutPropertyDefinition.PROPERTY_NAME, new TimeoutPropertyDefinition());        
        putSectionPropertyDefinition(UseLocalDesignSettingsPropertyDefinition.PROPERTY_NAME, new UseLocalDesignSettingsPropertyDefinition());        
        putSectionPropertyDefinition(UserDefinedCribPropertyDefinition.PROPERTY_NAME, new UserDefinedCribPropertyDefinition());        
        putSectionPropertyDefinition(UseXMLInputPropertyDefinition.PROPERTY_NAME, new UseXMLInputPropertyDefinition());        
    } 
    
    // sburns - not used!
    public String sort() {
        String value = "";
        Map<String, SectionPropertyDefinition> sectionPropertyDefintionMap = getSectionPropertyDefinitionMap();
        
        //TreeMap newMap = new TreeMap(sectionPropertyDefintionMap);
        //Collections.sort(newMap);
        
        Set<String> keySet = sectionPropertyDefintionMap.keySet();
        
        List<String> newList = new ArrayList<>(keySet); 
        Collections.sort(newList);
        
        int i = 0;
        
        for (String item: newList) {            
            if (i == 0) {
                value = item;
            } else {
                value = value + ", " + item;
            }
            
            i++;
        }
                
        return value;
    }
}
