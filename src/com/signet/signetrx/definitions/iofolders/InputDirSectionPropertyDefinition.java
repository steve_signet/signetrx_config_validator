/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.signet.signetrx.definitions.iofolders;

import com.signet.signetrx.definitions.ValidPathSectionPropertyDefinition;

/**
 *
 * @author sburns
 */
public class InputDirSectionPropertyDefinition extends ValidPathSectionPropertyDefinition {
    
    public static final String PROPERTY_NAME = "InputDir";
    public static final String DEFAULT_VALUE = "";
    public static final boolean ACCESS_CONTROLLED = true;
    
    public InputDirSectionPropertyDefinition() {
        super(PROPERTY_NAME, DEFAULT_VALUE, ACCESS_CONTROLLED);
    }
}
