/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.signet.signetrx.definitions.general;

import com.signet.signetrx.definitions.NumericRangeSectionPropertyDefinition;

/**
 *
 * @author sburns
 */
public class MaxConvexAddPropertyDefinition extends NumericRangeSectionPropertyDefinition {
    
    public static final String PROPERTY_NAME = "MaxConvexAdd";
    public static final String DEFAULT_VALUE = "5";
    public static final boolean ACCESS_CONTROLLED = false;
    
    public MaxConvexAddPropertyDefinition() {
        super(PROPERTY_NAME, DEFAULT_VALUE, ACCESS_CONTROLLED);
        this.setRange(-5, 5);
    }
}
