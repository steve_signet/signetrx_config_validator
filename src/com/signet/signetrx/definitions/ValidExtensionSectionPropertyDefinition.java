/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.signet.signetrx.definitions;

/**
 *
 * @author sburns
 */
public abstract class ValidExtensionSectionPropertyDefinition extends SectionPropertyDefinition {
    private String name;

    public ValidExtensionSectionPropertyDefinition(String name, String defaultValue, boolean accessControlled) {
        super(name, defaultValue, accessControlled);
        this.name = name;
    }
    
    @Override
    public String validate(String value) {
        String errorMessage = name + " section property: Expected valid filename extension, found %s";
        String regex = "[a-zA-Z0-9_]+";
        
        if (value.matches(regex)) {
            errorMessage = "";
        } else {
            errorMessage = String.format(errorMessage, value);            
        }
        return errorMessage;
    }   
}
