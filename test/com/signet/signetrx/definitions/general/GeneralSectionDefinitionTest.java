/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.signet.signetrx.definitions.general;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.ini4j.Ini;
import org.ini4j.Wini;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author sburns
 */
public class GeneralSectionDefinitionTest {
    private GeneralSectionDefinition instance;
    private Set<String> keySet;
    
    public GeneralSectionDefinitionTest() {
    }
    
    @Before
    public void setUp() {
        instance = new GeneralSectionDefinition();
        
        Wini ini;
        Set<String> sectionNameSet;
        
        try {
            String fileName = "test/com/signet/signetrx/validate/100130_Central_RxInfo.ini";

            ini = new Wini(new File(fileName));
        
            sectionNameSet = ini.keySet(); 

            for(String sectionName: sectionNameSet) {
                if (sectionName.equals("GENERAL")) {
                    Ini.Section section = ini.get(sectionName);
                    keySet = section.keySet();   
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(GeneralSectionDefinitionTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Test
    public void testGetInvalidSectionPropertyNameList() {
        System.out.println("testGetInvalidSectionPropertyNameList");

        List<String> expResult = new ArrayList<>();
        expResult.add("LowTimeWarning");
        expResult.add("SDFSlopeData");
        
        List<String> result = instance.getInvalidSectionPropertyNameList(keySet);
        
        assertEquals(expResult.get(0), result.get(0));
        assertEquals(expResult.get(1), result.get(1));
    }
    
    @Test
    public void testGetMissingSectionPropertyNameList() {
        System.out.println("validate");

        List<String> expResult = new ArrayList<>();
        expResult.add("UseXMLInput");
        expResult.add("LogFileFreq");
        
        List<String> result = instance.getMissingSectionPropertyNameList(keySet);
        
        assertEquals(expResult.get(0), result.get(0));
        assertEquals(expResult.get(1), result.get(1));
    }
}