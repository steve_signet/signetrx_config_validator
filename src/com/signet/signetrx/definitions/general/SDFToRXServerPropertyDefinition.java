/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.signet.signetrx.definitions.general;

import com.signet.signetrx.definitions.ListSectionPropertyDefinition;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author sburns
 */
public class SDFToRXServerPropertyDefinition extends ListSectionPropertyDefinition {
    
    public static final String PROPERTY_NAME = "SDFToRXServer";
    public static final String DEFAULT_VALUE = "0";
    public static final boolean ACCESS_CONTROLLED = true;
    
    public SDFToRXServerPropertyDefinition() {
        super(PROPERTY_NAME, DEFAULT_VALUE, ACCESS_CONTROLLED);
        List<String> list = new ArrayList<>();
        list.add("0");
        this.setList(list);
    }
}
