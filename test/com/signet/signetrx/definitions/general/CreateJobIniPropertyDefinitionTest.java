/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.signet.signetrx.definitions.general;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author sburns
 */
public class CreateJobIniPropertyDefinitionTest {
    private CreateJobIniPropertyDefinition instance;
    
    public CreateJobIniPropertyDefinitionTest() {
    }
       
    @Before
    public void setUp() {
        instance = new CreateJobIniPropertyDefinition();
    }
    
    @Test
    public void testValidateSucceed1() {
        System.out.println("validate");
        String value = "0";
        String expResult = "";
        String result = instance.validate(value);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testValidateSucceed2() {
        System.out.println("validate");
        String value = "1";
        String expResult = "";
        String result = instance.validate(value);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testValidateSucceed3() {
        System.out.println("validate");
        String value = "2";
        String expResult = "";
        String result = instance.validate(value);
        assertEquals(expResult, result);
    }    
    @Test
    public void testValidateFail() {
        System.out.println("validate");
        String value = "3";
        String expResult = "";
        String result = instance.validate(value);
        assertNotSame(expResult, result);
    }
}