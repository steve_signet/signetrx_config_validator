/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.signet.signetrx.definitions.general;

import com.signet.signetrx.definitions.NumericRangeSectionPropertyDefinition;

/**
 *
 * @author sburns
 */
public class TimeoutPropertyDefinition extends NumericRangeSectionPropertyDefinition {
    
    public static final String PROPERTY_NAME = "Timeout";
    public static final String DEFAULT_VALUE = "90";
    public static final boolean ACCESS_CONTROLLED = true;
    
    public TimeoutPropertyDefinition() {
        super(PROPERTY_NAME, DEFAULT_VALUE, ACCESS_CONTROLLED);
        this.setIncrement(1);
        this.setRange(0, 3600);
    }
}
