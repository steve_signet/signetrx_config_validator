/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.signet.signetrx.definitions;

/**
 *
 * @author sburns
 */
public abstract class ValidPathSectionPropertyDefinition extends SectionPropertyDefinition {
    private String name;

    public ValidPathSectionPropertyDefinition(String name, String defaultValue, boolean accessControlled) {
        super(name, defaultValue, accessControlled);
        this.name = name;
    }
    
    @Override
    public String validate(String value) {
        String errorMessage = name + " section property: Expected valid filename path, found %s";
        String regex1 = "[a-zA-Z]:\\\\(?:[^\\\\/:*?\"<>|\\r\\n]+\\\\)*[^\\\\/:*?\"<>|\\r\\n]*";
        String regex2 = "([a-zA-Z]:)?/(?:[^//:*?\"<>|\\r\\n]+/)*[^//:*?\"<>|\\r\\n]*";
        
        if (value.matches(regex1) || value.matches(regex2)) {
            errorMessage = "";
        } else {
            errorMessage = String.format(errorMessage, value);            
        }
        
        return errorMessage;
    }   
}