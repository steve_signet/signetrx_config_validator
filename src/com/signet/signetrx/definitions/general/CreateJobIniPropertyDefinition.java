/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.signet.signetrx.definitions.general;

import com.signet.signetrx.definitions.ListSectionPropertyDefinition;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author sburns
 */
public class CreateJobIniPropertyDefinition extends ListSectionPropertyDefinition {
    
    public static final String PROPERTY_NAME = "CreateJobIni";
    public static final String DEFAULT_VALUE = "2";
    public static final boolean ACCESS_CONTROLLED = true;
    
    private final List<String> list = new ArrayList<>();
    
    public CreateJobIniPropertyDefinition() {
        super(PROPERTY_NAME, DEFAULT_VALUE, ACCESS_CONTROLLED);
        list.add("0");
        list.add("1");
        list.add("2");
        setList(list);
    }
}
