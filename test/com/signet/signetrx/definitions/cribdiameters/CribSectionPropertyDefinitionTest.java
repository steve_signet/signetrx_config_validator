/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.signet.signetrx.definitions.cribdiameters;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author sburns
 */
public class CribSectionPropertyDefinitionTest {
    private CribSectionPropertyDefinition instance;
    
    public CribSectionPropertyDefinitionTest() {
    }
       
    @Before
    public void setUp() {
        String name = "crib1";
        instance = new CribSectionPropertyDefinition(name);
    }
    
    @Test
    public void testValidateSucceed() {
        System.out.println("validate");
        String value = "50.0;50.0";
        String expResult = "";
        String result = instance.validate(value);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testValidateFail() {
        System.out.println("validate");
        String value = "50.0;50.0x";
        String expResult = "";
        String result = instance.validate(value);
        assertNotSame(expResult, result);
    }
}