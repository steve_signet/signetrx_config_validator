/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.signet.signetrx.definitions.currency;

import com.signet.signetrx.definitions.ListSectionPropertyDefinition;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author sburns
 */
public class CurrencySectionPropertyDefinition extends ListSectionPropertyDefinition {
    
    public static final String PROPERTY_NAME = "CURRENCY";
    public static final String DEFAULT_VALUE = "$";
    public static final boolean ACCESS_CONTROLLED = true;
    
    public CurrencySectionPropertyDefinition() {
        super(PROPERTY_NAME, DEFAULT_VALUE, ACCESS_CONTROLLED);
        List<String> list = new ArrayList<>();
        list.add("$");
        this.setList(list);
    }
}
