/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.signet.signetrx.definitions.iofolders;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author sburns
 */
public class OutputDirSectionPropertyDefinitionTest {
    private OutputDirSectionPropertyDefinition instance;
    
    public OutputDirSectionPropertyDefinitionTest() {
    }
    
    @Before
    public void setUp() {
        instance = new OutputDirSectionPropertyDefinition();
    }
    
    @Test
    public void testValidateSucceed1() {
        System.out.println("validate");
        String value = "C:/Users/sbaker/repos";
        String expResult = "";
        String result = instance.validate(value);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testValidateSucceed2() {
        System.out.println("validate");
        String value = "/Users/sbaker/repos";
        String expResult = "";
        String result = instance.validate(value);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testValidateFail() {
        System.out.println("validate");
        String value = "C:C:/Users/sbaker/repos";
        String expResult = "";
        String result = instance.validate(value);
        assertNotNull(expResult, result);
    }
}