/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.signet.signetrx.definitions;

/**
 *
 * @author sburns
 */
public abstract class SectionPropertyDefinition {
    private String name;
    private String defaultValue;
    private boolean accessControlled;
    
    public SectionPropertyDefinition(String name, String defaultValue, boolean accessControlled) {
        this.name = name;
        this.defaultValue = defaultValue;
        this.accessControlled = accessControlled;
    }
    
    public abstract String validate(String value);   

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the defaultValue
     */
    public String getDefaultValue() {
        return defaultValue;
    }

    /**
     * @param defaultValue the defaultValue to set
     */
    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    /**
     * @return the accessControlled
     */
    public boolean isAccessControlled() {
        return accessControlled;
    }

    /**
     * @param accessControlled the accessControlled to set
     */
    public void setAccessControlled(boolean accessControlled) {
        this.accessControlled = accessControlled;
    }
}
