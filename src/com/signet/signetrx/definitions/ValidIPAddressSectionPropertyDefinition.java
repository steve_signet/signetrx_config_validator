/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.signet.signetrx.definitions;

/**
 *
 * @author sburns
 */
public abstract class ValidIPAddressSectionPropertyDefinition extends SectionPropertyDefinition {
    private String name;

    public ValidIPAddressSectionPropertyDefinition(String name, String defaultValue, boolean accessControlled) {
        super(name, defaultValue, accessControlled);
        this.name = name;
    }
    
    @Override
    public String validate(String value) {
        String errorMessage = name + " section property: Expected valid IP address, found %s";
        String regex = "(?:[0-9]{1,3}\\.){3}[0-9]{1,3}";
        
        if (value.matches(regex)) {
            errorMessage = "";
        } else {
            errorMessage = String.format(errorMessage, value);            
        }
        return errorMessage;
    }   
}
