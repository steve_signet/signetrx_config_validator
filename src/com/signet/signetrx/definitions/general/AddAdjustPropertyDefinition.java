/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.signet.signetrx.definitions.general;

import com.signet.signetrx.definitions.ListSectionPropertyDefinition;

/**
 *
 * @author sburns
 */
public class AddAdjustPropertyDefinition extends ListSectionPropertyDefinition {
    
    public static final String PROPERTY_NAME = "AddAdjust";
    public static final String DEFAULT_VALUE = "0";
    public static final boolean ACCESS_CONTROLLED = false;
    
    public AddAdjustPropertyDefinition() {
        super(PROPERTY_NAME, DEFAULT_VALUE, ACCESS_CONTROLLED);
    }
}
