/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.signet.signetrx.definitions;

/**
 *
 * @author sburns
 */
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public abstract class SectionDefinition {

    private String sectionDefinitionName;
    Map<String, SectionPropertyDefinition> sectionPropertyDefinitionMap = new HashMap<>();
    
    public SectionDefinition(String sectionDefinitionName) {
        this.sectionDefinitionName = sectionDefinitionName;
    }
    
    public final String getSectionDefinitionName() {
        return "[" + sectionDefinitionName + "]";
    }
    
    public final Map<String, SectionPropertyDefinition> getSectionPropertyDefinitionMap() {
        return sectionPropertyDefinitionMap;
    }
        
    public void putSectionPropertyDefinition(String sectionPropertyName, SectionPropertyDefinition sectionPropertyDefinition) {
        sectionPropertyDefinitionMap.put(sectionPropertyName, sectionPropertyDefinition);
    }
           
    /**
     * Checks if there are any invalid section property names in the keySet
     * If there invalid section property names they are printed out
     * 
     * @param keySet
     * @param sectionPropertyNameList 
     */
    public List<String> getInvalidSectionPropertyNameList(Set<String> keySet) {
        List<String> invalidSectionPropertyNameList = new ArrayList<>();
        
        sectionPropertyDefinitionMap = getSectionPropertyDefinitionMap();        
        
        for (String key: keySet) {               
            boolean isValidKey = sectionPropertyDefinitionMap.containsKey(key);

            if (isValidKey == false) {
                invalidSectionPropertyNameList.add(key);
            }
        }
        
        return invalidSectionPropertyNameList;
    }
       
    /**
     * Checks if there are any missing section property names in the keySet
     * If there missing section property names they are printed out
     * 
     * @param keySet
     * @param sectionPropertyNameList 
     */
    public List<String> getMissingSectionPropertyNameList(Set<String> keySet) {
        List<String> missingSectionPropertyNameList = new ArrayList<>();
                        
        for (String sectionPropertyDefinitionKey: sectionPropertyDefinitionMap.keySet()) {
            if (keySet.contains(sectionPropertyDefinitionKey) == false) {
                missingSectionPropertyNameList.add(sectionPropertyDefinitionKey);
            }
        }
        
        return missingSectionPropertyNameList;
    }    
        
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getSectionDefinitionName()).append("\n");
        
        Set<String> sectionPropertyKeySet = sectionPropertyDefinitionMap.keySet();
        
        for (String key : sectionPropertyKeySet) {
            SectionPropertyDefinition sectionPropertyDefinition = sectionPropertyDefinitionMap.get(key);
            String value = sectionPropertyDefinition.getName();
            sb.append(key).append("\n").append(value);
        }
        return sb.toString();
    }
}

