/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.signet.signetrx.definitions.currency;

import com.signet.signetrx.definitions.SectionDefinition;

/**
 *
 * @author sburns
 */
public class CurrencySectionDefinition extends SectionDefinition {
    public CurrencySectionDefinition() {
        super("DESIGNS");
        
        putSectionPropertyDefinition(CurrencySectionPropertyDefinition.PROPERTY_NAME, new CurrencySectionPropertyDefinition());
    }
}