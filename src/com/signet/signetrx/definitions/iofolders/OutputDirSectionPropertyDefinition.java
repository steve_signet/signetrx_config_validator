/**
 *
 * @author sburns
 */
package com.signet.signetrx.definitions.iofolders;

import com.signet.signetrx.definitions.ValidPathSectionPropertyDefinition;

/**
 *
 * @author sburns
 */
public class OutputDirSectionPropertyDefinition extends ValidPathSectionPropertyDefinition {
    
    public static final String PROPERTY_NAME = "OutputDir";
    public static final String DEFAULT_VALUE = "";
    public static final boolean ACCESS_CONTROLLED = true;
    
    public OutputDirSectionPropertyDefinition() {
        super(PROPERTY_NAME, DEFAULT_VALUE, ACCESS_CONTROLLED);
    }
}
