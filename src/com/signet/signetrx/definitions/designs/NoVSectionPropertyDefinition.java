/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.signet.signetrx.definitions.designs;

import com.signet.signetrx.definitions.ListSectionPropertyDefinition;

/**
 *
 * @author sburns
 */
public class NoVSectionPropertyDefinition extends ListSectionPropertyDefinition {
    
    public static final String PROPERTY_NAME = "NO_V";
    public static final String DEFAULT_VALUE = "0";
    public static final boolean ACCESS_CONTROLLED = true;
    
    public NoVSectionPropertyDefinition() {
        super(PROPERTY_NAME, DEFAULT_VALUE, ACCESS_CONTROLLED);
    }
}
