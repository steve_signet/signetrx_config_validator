/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.signet.signetrx.definitions.general;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author sburns
 */
public class SmdMaxThresholdPropertyDefinitionTest {
    private SmdMaxThresholdPropertyDefinition instance;
    
    public SmdMaxThresholdPropertyDefinitionTest() {
    }
    
    @Before
    public void setUp() {
        instance = new SmdMaxThresholdPropertyDefinition();
    }

    /**
     * Test of validate method, of class NumericRangeSectionPropertyDefinition.
     */
    @Test
    public void testValidValue1() {
        System.out.println("validate");
        String value = "2.5";
                       
        String expResult = "";
        String result = instance.validate(value);
        assertEquals(expResult, result);
    }

    /**
     * Test of validate method, of class NumericRangeSectionPropertyDefinition.
     */
    @Test
    public void testValidValue2() {
        System.out.println("validate");
        String value = "-2.5";
                       
        String expResult = "";
        String result = instance.validate(value);
        assertEquals(expResult, result);
    }

    /**
     * Test of validate method, of class NumericRangeSectionPropertyDefinition.
     */
    @Test
    public void testValueLessThanMinimum() {
        System.out.println("validate");
        String value = "-2000";
        
        String expResult = "";
        String result = instance.validate(value);
        assertNotSame(expResult, result);
    }

    /**
     * Test of validate method, of class NumericRangeSectionPropertyDefinition.
     */
    @Test
    public void testValueGreaterThanMaximum() {
        System.out.println("validate");
        String value = "2000.0";
        
        String expResult = "";
        String result = instance.validate(value);
        assertNotSame(expResult, result);
    }
}