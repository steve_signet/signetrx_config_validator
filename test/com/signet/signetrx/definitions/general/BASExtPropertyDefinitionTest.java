/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.signet.signetrx.definitions.general;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author sburns
 */
public class BASExtPropertyDefinitionTest {
    private BASExtPropertyDefinition instance;
    
    public BASExtPropertyDefinitionTest() {
    }
       
    @Before
    public void setUp() {
        instance = new BASExtPropertyDefinition();
    }
    
    @Test
    public void testValidateSucceed() {
        System.out.println("validate");
        String value = "lms";
        String expResult = "";
        String result = instance.validate(value);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testValidateSucceed2() {
        System.out.println("validate");
        String value = "lms1";
        String expResult = "";
        String result = instance.validate(value);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testValidateFail() {
        System.out.println("validate");
        String value = "lms%";
        String expResult = "";
        String result = instance.validate(value);
        assertNotSame(expResult, result);
    }
}