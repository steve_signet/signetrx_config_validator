/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.signet.signetrx.definitions;

/**
 *
 * @author sburns
 */
public abstract class NumericRangeSectionPropertyDefinition extends SectionPropertyDefinition {
    private String name;    
    private double min, max, increment;

    public NumericRangeSectionPropertyDefinition(String name, String defaultValue, boolean accessControlled) {
        super(name, defaultValue, accessControlled);
        this.min = 0;
        this.max = 1000;
        this.increment = 0;
    }
    
    public void setRange(double min, double max) {
        this.min = min;
        this.max = max;
    }
    
    public void setIncrement(double increment) {
        this.increment = increment;
    }
    
    @Override
    public String validate(String value) {
        String errorMessage = "";
        
        double doubleValue = Double.valueOf(value);
        
        if (doubleValue < min) {
            errorMessage = name + " section property: value less than %s, found %s";
            errorMessage = String.format(errorMessage, min, doubleValue);            
        } else if (doubleValue > max) {
            errorMessage = name + " section property: value greater than %s, found %s";
            errorMessage = String.format(errorMessage, max, doubleValue);            
        }
        
        if (increment > 0) {
            double remainder = Math.abs(doubleValue % increment);
            
            if (remainder > 0) {
                errorMessage = name + " section property: value %s is not an increment of %s";
                errorMessage = String.format(errorMessage, doubleValue, increment);            
            }
        }
 
        return errorMessage;
    }   
}

