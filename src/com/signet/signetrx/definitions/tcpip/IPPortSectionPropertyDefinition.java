/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.signet.signetrx.definitions.tcpip;

import com.signet.signetrx.definitions.NumericRangeSectionPropertyDefinition;

/**
 *
 * @author sburns
 */
public class IPPortSectionPropertyDefinition extends NumericRangeSectionPropertyDefinition {
    
    public static final String PROPERTY_NAME = "IPPort";
    public static final String DEFAULT_VALUE = "1100";
    public static final boolean ACCESS_CONTROLLED = true;
    
    public IPPortSectionPropertyDefinition() {
        super(PROPERTY_NAME, DEFAULT_VALUE, ACCESS_CONTROLLED);
        this.setRange(0, 65535);
        this.setIncrement(1);
    }
}
