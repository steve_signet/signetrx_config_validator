/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.signet.signetrx.definitions.exprism;

import com.signet.signetrx.definitions.SectionDefinition;

/**
 *
 * @author sburns
 */
public class ExPrismSectionDefinition extends SectionDefinition {
    public ExPrismSectionDefinition() {
        super("EXPRISM");
        
        putSectionPropertyDefinition("0.75,0.275", new ExPrismSectionPropertyDefinition());
        putSectionPropertyDefinition("1.00,0.275", new ExPrismSectionPropertyDefinition());
        putSectionPropertyDefinition("1.25,0.300", new ExPrismSectionPropertyDefinition());
        putSectionPropertyDefinition("1.50,0.312", new ExPrismSectionPropertyDefinition());
        putSectionPropertyDefinition("1.75,0.325", new ExPrismSectionPropertyDefinition());
        putSectionPropertyDefinition("2.00,0.350", new ExPrismSectionPropertyDefinition());
        putSectionPropertyDefinition("2.25,0.375", new ExPrismSectionPropertyDefinition());
        putSectionPropertyDefinition("2.50,0.380", new ExPrismSectionPropertyDefinition());
        putSectionPropertyDefinition("2.75,0.380", new ExPrismSectionPropertyDefinition());
        putSectionPropertyDefinition("3.00,0.380", new ExPrismSectionPropertyDefinition());
        putSectionPropertyDefinition("3.25,0.380", new ExPrismSectionPropertyDefinition());
        putSectionPropertyDefinition("3.50,0.380", new ExPrismSectionPropertyDefinition());
    }
}
