/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.signet.signetrx.definitions.cribdiameters;

import com.signet.signetrx.definitions.SectionDefinition;

/**
 *
 * @author sburns
 */
public class CribDiametersSectionDefinition extends SectionDefinition {
    private String name;
                
    public CribDiametersSectionDefinition(int number) {
        super("CRIB DIAMETERS");
        
        for (int i = 1; i <= number; i++) {
            name = "crib" + i;
            putSectionPropertyDefinition(name, new CribSectionPropertyDefinition(name));
        }
    }
}
