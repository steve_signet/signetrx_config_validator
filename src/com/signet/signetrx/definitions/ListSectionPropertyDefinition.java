/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.signet.signetrx.definitions;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author sburns
 */
public abstract class ListSectionPropertyDefinition extends SectionPropertyDefinition {
    private String name;
    private List<String> list = new ArrayList<>();
    
    public ListSectionPropertyDefinition(String name, String defaultValue, boolean accessControlled) {
        super(name, defaultValue, accessControlled); 
        this.name = name;
        list.add("0");
        list.add("1");
    }
    
    public void setList(List<String> list) {
        this.list = list;
    }
    
    @Override
    public String validate(String value) {
        String items = "";
        
        int i = 0;
        
        for (String item: list) {
            if (i == 0) {
                items = item;
            } else if (i == (list.size() - 1)) {
                items = items + " or " + item;
            } else {
                items = items + ", " + item;
            }
            
            i++;
        }
        
        String errorMessage = name + " section property: Expected %s, found %s";
                
        if (list.contains(value)) {
            errorMessage = "";
        } else {
            errorMessage = String.format(errorMessage, items, value);            
        }
        
        return errorMessage;
    }   
}