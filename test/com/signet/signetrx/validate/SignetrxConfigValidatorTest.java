/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.signet.signetrx.validate;

import com.signet.signetrx.definitions.general.GeneralSectionDefinition;
import com.signet.signetrx.definitions.general.GeneralSectionDefinitionTest;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.ini4j.Ini;
import org.ini4j.Wini;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author sburns
 */
public class SignetrxConfigValidatorTest {
    private SignetrxConfigValidator signetrxConfigValidator;
    private Ini.Section section;
    private GeneralSectionDefinition generalSectionDefinition;
    
    public SignetrxConfigValidatorTest() {
    }
    
    @Before
    public void setUp() {
        String fileName = "test/com/signet/signetrx/validate/100130_Central_RxInfo.ini";
        signetrxConfigValidator = new SignetrxConfigValidator(fileName);
        generalSectionDefinition = new GeneralSectionDefinition();
        
        Wini ini;
        Set<String> sectionNameSet;

        try {
            ini = new Wini(new File(fileName));

            sectionNameSet = ini.keySet(); 

            for(String sectionName: sectionNameSet) {
                if (sectionName.equals("GENERAL")) {
                    section = ini.get(sectionName);
                    break;
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(GeneralSectionDefinitionTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
     
    /**
     * Test of run method, of class SignetrxConfigValidator.
     */
    @Test
    public void testRun() {
        System.out.println("run");
        signetrxConfigValidator.run();
    }

    /**
     * Test of run method, of class SignetrxConfigValidator.
     */
    @Test
    public void testValidate() {
        System.out.println("validate");
        
        List<String> errorMessageList = signetrxConfigValidator.validate(generalSectionDefinition, section);
        
        List<String> expErrorMessageList = new ArrayList<>();
        expErrorMessageList.add("AddAdjust section property: Expected 0 or 1, found 1xx");        
        
        assertEquals(expErrorMessageList.get(0), errorMessageList.get(0));
    }
}