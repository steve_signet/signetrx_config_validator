/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.signet.signetrx.definitions.general;

import com.signet.signetrx.definitions.NumericRangeSectionPropertyDefinition;

/**
 *
 * @author sburns
 */
public class SmdMaxThresholdPropertyDefinition extends NumericRangeSectionPropertyDefinition {
    
    public static final String PROPERTY_NAME = "SmdMaxThreshold";
    public static final String DEFAULT_VALUE = "-20";
    public static final boolean ACCESS_CONTROLLED = true;
    
    public SmdMaxThresholdPropertyDefinition() {
        super(PROPERTY_NAME, DEFAULT_VALUE, ACCESS_CONTROLLED);
        this.setRange(-20, 20);
    }
}
