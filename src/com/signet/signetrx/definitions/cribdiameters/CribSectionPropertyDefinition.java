/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.signet.signetrx.definitions.cribdiameters;

import com.signet.signetrx.definitions.SectionPropertyDefinition;

/**
 *
 * @author sburns
 */
public class CribSectionPropertyDefinition extends SectionPropertyDefinition {
    
    public static final String PROPERTY_NAME = "crib1";
    public static final String DEFAULT_VALUE = "crib1";
    public static final boolean ACCESS_CONTROLLED = true;
    
    private String name;
    
    public CribSectionPropertyDefinition(String name) {
        super(PROPERTY_NAME, DEFAULT_VALUE, ACCESS_CONTROLLED);
        this.name = name;
    }

    @Override
    public String validate(String value) {
        String errorMessage = name + " section property: Expected %s, found %s";
        
        String regex = "[0-9]{1,2}\\.[0-9]{1};[0-9]{1,2}\\.[0-9]{1}";
        
        if (value.matches(regex) == true) {
            errorMessage = "";
        } else {
            errorMessage = String.format(errorMessage, "crib[x] = [xx.x;xx.x]", value);            
        }
                
        return errorMessage;
    }
}
