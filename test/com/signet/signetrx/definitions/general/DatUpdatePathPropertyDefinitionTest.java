/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.signet.signetrx.definitions.general;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author sburns
 */
public class DatUpdatePathPropertyDefinitionTest {
    private DatUpdatePathPropertyDefinition instance;
    
    public DatUpdatePathPropertyDefinitionTest() {
    }
    
    @Before
    public void setUp() {
        instance = new DatUpdatePathPropertyDefinition();
    }
    
    @Test
    public void testValidateSucceed1() {
        System.out.println("validate");
        String value = "C:\\Users\\sbaker\\repos";
        String expResult = "";
        String result = instance.validate(value);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testValidateSucceed2() {
        System.out.println("validate");
        String value = "C:/Users/sbaker/repos";
        String expResult = "";
        String result = instance.validate(value);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testValidateSucceed3() {
        System.out.println("validate");
        String value = "/Users/sbaker/repos";
        String expResult = "";
        String result = instance.validate(value);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testValidateFail() {
        System.out.println("validate");
        String value = "/Users/sbaker/repos";
        String expResult = "";
        String result = instance.validate(value);
        assertNotNull(expResult, result);
    }
}