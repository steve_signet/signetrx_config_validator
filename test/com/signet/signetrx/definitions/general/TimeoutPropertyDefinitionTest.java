/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.signet.signetrx.definitions.general;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author sburns
 */
public class TimeoutPropertyDefinitionTest {
    private TimeoutPropertyDefinition instance;
    
    public TimeoutPropertyDefinitionTest() {
    }
    
    @Before
    public void setUp() {
        instance = new TimeoutPropertyDefinition();
    }

    /**
     * Test of validate method, of class NumericRangeSectionPropertyDefinition.
     */
    @Test
    public void testValidValue() {
        System.out.println("validate");
        String value = "80";
                       
        String expResult = "";
        String result = instance.validate(value);
        assertEquals(expResult, result);
    }

    /**
     * Test of validate method, of class NumericRangeSectionPropertyDefinition.
     */
    @Test
    public void testValueLessThanMinimum() {
        System.out.println("validate");
        String value = "-1";
        
        String expResult = "";
        String result = instance.validate(value);
        assertNotSame(expResult, result);
    }

    /**
     * Test of validate method, of class NumericRangeSectionPropertyDefinition.
     */
    @Test
    public void testValueGreaterThanMaximum() {
        System.out.println("validate");
        String value = "3601";
        
        String expResult = "";
        String result = instance.validate(value);
        assertNotSame(expResult, result);
    }

    /**
     * Test of validate method, of class NumericRangeSectionPropertyDefinition.
     */
    @Test
    public void testInvalidIncrement() {
        System.out.println("validate");
        String value = "63.5";
        
        String expResult = "";
        String result = instance.validate(value);
        assertNotSame(expResult, result);
    }
}