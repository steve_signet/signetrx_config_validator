/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.signet.signetrx.definitions.general;

import com.signet.signetrx.definitions.ValidPathSectionPropertyDefinition;

/**
 *
 * @author sburns
 */
public class DatUpdatePathPropertyDefinition extends ValidPathSectionPropertyDefinition {
    
    public static final String PROPERTY_NAME = "DatUpdatePath";
    public static final String DEFAULT_VALUE = "C:\\";
    public static final boolean ACCESS_CONTROLLED = true;
    
    public DatUpdatePathPropertyDefinition() {
        super(PROPERTY_NAME, DEFAULT_VALUE, ACCESS_CONTROLLED);
    }
}
